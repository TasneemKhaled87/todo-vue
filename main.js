var filters = {
    'all': function(todos){
      return todos;
    },

    active: function(todos){
      return todos.filter(function(todo){
        return ! todo.completed;
      });
    },

    completed: function(todos){
      return todos.filter(function(todo){
        return todo.completed;
      });
    }

  }


new Vue({
    'el': '.todoapp',
    data: {

  //filter
  visibility: 'all',
//edit todo

  editingTodo: null,
  oldEditingTodoTitle: null,

 //add task
    newTodo:'',

      todos: [
        {title:'test 1', completed: true},
        {title:'test 2', completed: false},
      ]
    },


 //filter
 
   computed:{
      filterdTodos: function(){
      return filters[ this.visibility ] (this.todos);
    },


 // items 
  
 remaningTodos: function(){
    return filters.active( this.todos ).length;
  },
  
  remaningText: function(){
    if( filters.active( this.todos ).length > 1)
      return 'items';

    return 'item';
  },



//the arrow 

allDone: {
  get: function(){
    return this.remaningTodos === 0;
  },

  set: function(value){
    this.todos.forEach(function(todo){
      todo.completed = value;
    });
  }
}

},



    methods:{
//delete task
      deleteTodo: function(todo){
        this.todos.splice(this.todos.indexOf(todo), 1);
      },
//add task
      addTodo:function(todo){
          this.todos.push({
              'title' :this.newTodo,
              'completed' :false
          });

          this.newTodo='';

      },
      
       
 //remove task 
 removeCompleted(){
  this.todos = filters.active( this.todos );
},


//edit task

editTodo(todo){
  this.editingTodo = todo;
  this.oldEditingTodoTitle = todo.title;
},
doneEditing(){
  if(this.editingTodo.title == '')
    this.deleteTodo(this.editingTodo);

  this.editingTodo = null;
},
cancelEditing(){
  this.editingTodo.title = this.oldEditingTodoTitle;
  this.editingTodo = null;
}


}
});